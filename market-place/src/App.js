import React from "react";
import "./App.css";
import Layout from "./Layout/Layout";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/" component={Layout} />
        <Route exact path="/:id" component={Layout} />
        <Route exact path="/*" component={() => "NOT FOUND"} />
      </Switch>
    </Router>
  );
}

export default App;
