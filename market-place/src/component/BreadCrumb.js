import React from "react";
import Typography from "@material-ui/core/Typography";
import NavigateNextIcon from "@material-ui/icons/NavigateNext";
import Breadcrumbs from "@material-ui/core/Breadcrumbs";
import { Link } from "react-router-dom";

export default function BreadCrumb(props) {
  const { breadcrumb } = props;
  console.log(breadcrumb);
  return (
    <Breadcrumbs
      separator={<NavigateNextIcon fontSize="small" />}
      aria-label="breadcrumb"
    >
      <Typography color="textPrimary">Market-Place</Typography>
      {breadcrumb.map((data) => (
        <React.Fragment>
          {data.link ? (
            <Link
              style={{
                cursor: "pointer",
                textDecoration: "none",
                color: "rgba(0, 0, 0, 0.54)",
              }}
              to={data.link}
            >
              {data.label}
            </Link>
          ) : (
            <Typography color="textPrimary">{data.label}</Typography>
          )}
        </React.Fragment>
      ))}
    </Breadcrumbs>
  );
}
