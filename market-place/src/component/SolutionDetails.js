import React from "react";
import Paper from "@material-ui/core/Paper";
import SendOutlined from "@material-ui/icons/SendOutlined";
import Button from "@material-ui/core/Button";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import Brightness1Icon from "@material-ui/icons/Brightness1";
import ListItemText from "@material-ui/core/ListItemText";
import Grid from "@material-ui/core/Grid";
import { Typography } from "@material-ui/core";

const styles = {
  width: "95%",
  marginLeft: 8,
};
const SolutionDetails = (props) => {
  const { solution } = props;
  return (
    <Paper style={styles} elevation={3} spacing={1}>
      <div style={{ padding: 8 }}>
        <div
          style={{
            color: "#004d40",
            marginTop: 15,
            marginBottom: 8,
            padding: 8,
          }}
        >
          {solution.solutionName}
        </div>
        <Grid container justify="left">
          <Grid item xs={8}>
            <Typography
              style={{ padding: 8 }}
              variant="body2"
              display="block"
              gutterBottom
            >
              {solution.solutionFullDescription}
            </Typography>
          </Grid>
          <Grid item xs={3} justify="center">
            <img
              src={require("../assets/images/lti_mosaic_marketplace.svg")}
              style={{
                paddingLeft: "8px",
                alignItems: "center",
              }}
              width={200}
            />
          </Grid>
        </Grid>
        <Grid container justify="left">
          <Grid item xs={4}>
            <div
              style={{
                color: "#004d40",
                marginTop: 15,
                marginBottom: 8,
                padding: 8,
              }}
            >
              {"Input Data Sources"}
            </div>
            <List component="nav" aria-label="main mailbox folders">
              {solution.inputDataSources.map((data) => {
                return (
                  <ListItem button style={{ paddingTop: 0, paddindBottom: 0 }}>
                    <ListItemIcon>
                      <Brightness1Icon style={{ fontSize: 10 }} />
                    </ListItemIcon>
                    <ListItemText primary={data} />
                  </ListItem>
                );
              })}
            </List>
          </Grid>
          <Grid item xs={4}>
            <div
              style={{
                color: "#004d40",
                marginTop: 15,
                marginBottom: 8,
                padding: 8,
              }}
            >
              {"Processing"}
            </div>
            <List component="nav" aria-label="main mailbox folders">
              {solution.processing.map((data) => {
                return (
                  <ListItem button style={{ paddingTop: 0, paddindBottom: 0 }}>
                    <ListItemIcon>
                      <Brightness1Icon style={{ fontSize: 10 }} />
                    </ListItemIcon>
                    <ListItemText primary={data} />
                  </ListItem>
                );
              })}
            </List>
          </Grid>
          <Grid item xs={4}>
            <div
              style={{
                color: "#004d40",
                marginTop: 15,
                marginBottom: 8,
                padding: 8,
              }}
            >
              {"Outcomes"}
            </div>
            <List component="nav" aria-label="main mailbox folders">
              {solution.outcomes.map((data) => {
                return (
                  <ListItem button style={{ paddingTop: 0, paddindBottom: 0 }}>
                    <ListItemIcon>
                      <Brightness1Icon style={{ fontSize: 10 }} />
                    </ListItemIcon>
                    <ListItemText primary={data} dense />
                  </ListItem>
                );
              })}
            </List>
          </Grid>
        </Grid>
        <Grid
          container
          direction="row"
          justify="flex-end"
          alignItems="flex-end"
          spacing={1}
        >
          <Grid item>
            <Button variant="outlined" endIcon={<SendOutlined />}>
              I Am Interested
            </Button>
          </Grid>
          <Grid item>
            <Button variant="outlined" endIcon={<SendOutlined />}>
              Launch
            </Button>
          </Grid>
        </Grid>
      </div>
    </Paper>
  );
};

export default SolutionDetails;
