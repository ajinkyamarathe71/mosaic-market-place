import React, { Component } from "react";
import SideBar from "./SideBar";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import logo from "../logo.svg";
import Box from "@material-ui/core/Box";
import { Typography } from "@material-ui/core";
import { Link } from "react-router-dom";

const Styles = {
  textAlign: "center",
  color: "black",
  height: 100,
  paddingTop: 20,
  cursor: "pointer",
};
export default class Category extends Component {
  render() {
    const { categories , typeName} = this.props;
    return (
      <Grid container spacing={2} justify="center" style={{ width: "100%" }}>
        <Grid container justify="center" > 
        <Typography  align ="center" variant="h4" style={{marginBottom:"2rem" ,fontWeight: "500", color:"#666"}}>{typeName}</Typography>  
        </Grid>
        {categories.map((category) => {
          const data = category.iconImage.data;
          return (
            <Grid key={category.stringId} item xs={3}>
              <Link
                to={"/" + category.stringId}
                style={{ textDecoration: "none" }}
              >
                <Paper elevation={3} style={Styles} spacing={1}>
                  <img
                    src={`data:image/jpeg;base64,${data}`}
                    alt="description of image"
                    width={45}
                    height={45}
                  />
                  <div style={{ color: "#004d40", marginTop: 15 }}>
                    {category.categoryName}
                  </div>
                </Paper>
              </Link>
            </Grid>
          );
        })}
      </Grid>
    );
  }
}
